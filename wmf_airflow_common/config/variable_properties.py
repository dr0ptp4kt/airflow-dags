"""
===========================================================================
THIS MECHANISM IS DEPRECATED.
PLEASE, USE wmf_airflow_common.config.dag_properties.DagProperties INSTEAD.
===========================================================================

This module helps overriding DAG properties dynamically,
altering them without changing the DAG file in version control.
To achieve that, it uses Airflow Variables. Variables can be populated
from Airflow's UI and also from the CLI. And they can be accessed from
Airflow code (i.e. within a DAG file).

=== To set up a variable in Airflow's UI:
 * In the UI, click on Admin > Variables.
 * Add a new record (Key is the variable name, Val is the value).
This way of altering DAG configs might be useful when i.e. re-running
a production DAG with a different start_date.

=== To set up a variable in the CLI:
 * Export an environment variable in the same context where Airflow runs.
 * The env var name should be 'AIRFLOW_VAR_<your_variable_name>'.
This way of altering DAG configs might be useful when testing a DAG while
developing it.

=== Variable format:
For this module to work, the variable value must be a JSON string.
For instance: '{"prop1": "val1", "prop2": {"prop3": "val3"}}'
If the specified variable exists but it's not a JSON string,
the code will raise a ValueError.

=== Add the code to your DAG file:
In your DAG file create a VariableProperties instance with:
    var_props = VariableProperties('<your_variable_name>')
And use it when passing properties to i.e. DAG, Sensors and Operators with:
    var_props.get('<property_name>', '<default_value>')
Where <property_name> is the property key within the Variable's JSON value,
and <default_value> is the value that var_props.get() will return in case
the Variable is not defined, or its JSON does not contain such property.

=== Precedence of property values:
When you ask for a property value with `var_props.get()`,
this module will try and return the value in this precedence order.
 * The value specified via CLI variable (if defined).
 * The value specified via Airflow UI (if defined).
 * The value specified as a default when calling VariableProperties(...).

=== Special property values
If the propery that you want to override is of type datetime, timedelta or dict,
use the following getters instead of var_props.get():

 * var_props.get_datetime(property_name, default_value):
   Expects the corresponding value stored in the Variable JSON to be an ISO8601
   timestamp ("2022-02-01T00"), and will raise a ValueError otherwise. It will
   parse it and return a datetime object. The default value must be a datetime.

 * var_props.get_timedelta(property_name, default_value):
   Expects the corresponding value stored in the Variable JSON to be an ISO8601
   duration ("P7DT4H"), and will raise a ValueError otherwise. It will parse it
   and return a timedelta object. The default value must be a timedelta.

 * var_props.get_merged(property_name, default_value):
   Expects the corresponding value stored in the Variable JSON to be a JSON
   object, and will raise a ValueError otherwise. It will parse it and return
   a dict containing all fields of the default_value plus all fields of the
   Variable JSON, potentially overriding the first with the latter.
   The default value should be a python dict.
"""

from datetime import datetime, timedelta
from json import JSONDecodeError
from typing import Any, Callable, cast
from warnings import warn

from airflow.models import Variable
from isodate import parse_duration


def no_parsing(x):
    return x


class VariableProperties:
    def __init__(self, variable_name: str):
        warn(
            "VariableProperties is deprecated. Please, use DagProperties instead.",
            DeprecationWarning,
        )
        try:
            self.variable: Variable = Variable.get(
                variable_name,
                deserialize_json=True,
                default_var={},
            )
        except JSONDecodeError:
            raise ValueError(f"Variable {variable_name} can not be parsed as JSON.")

    def get(self, property_name: str, default_value: Any) -> Any:
        return self.variable.get(property_name, default_value)

    def get_datetime(self, property_name: str, default_value: datetime) -> datetime:
        if type(default_value) is not datetime:
            raise ValueError("Default value is not a datetime.")
        return cast(datetime, self.get_parsed(property_name, datetime.fromisoformat, default_value))

    def get_list(self, property_name: str, default_value: list) -> list:
        if type(default_value) is not list:
            raise ValueError("Default value is not a list.")
        result = cast(list, self.get_parsed(property_name, no_parsing, default_value))
        if type(result) is not list:
            raise ValueError("Value is not a list.")
        return result

    def get_timedelta(self, property_name: str, default_value: timedelta) -> timedelta:
        if type(default_value) is not timedelta:
            raise ValueError("Default value is not a timedelta.")
        return cast(timedelta, self.get_parsed(property_name, parse_duration, default_value))

    def get_parsed(self, property_name: str, parser: Callable[[Any], Any], default_value: Any) -> Any:
        if property_name not in self.variable:
            return default_value
        try:
            return parser(self.variable[property_name])
        except ValueError:
            raise ValueError(f"Property {property_name} can not be parsed.")

    def get_merged(self, property_name: str, default_value: dict[str, Any]) -> dict[str, Any]:
        if type(default_value) is not dict:
            raise ValueError("Default value is not a dict.")
        if property_name not in self.variable:
            return default_value
        try:
            return {**default_value, **self.variable[property_name]}
        except TypeError:
            raise TypeError(f"Property {property_name} is not a json object.")

from typing import Any

from airflow.exceptions import AirflowFailException
from airflow.models import BaseOperator


class FailOperator(BaseOperator):
    """
    Operator failing a task.
    This is usefull to fail a job-instance when a thread of tasks
    is executed succesfully but represents an error.
    """

    def __init__(self, *, message: str, **kwargs: Any):
        """
        :param message:
            the message to report when failing
        """
        self._message = message

        super().__init__(**kwargs)

    def execute(self, context: Any) -> None:
        # Raise a failure-without-retry exception
        raise AirflowFailException(self._message)

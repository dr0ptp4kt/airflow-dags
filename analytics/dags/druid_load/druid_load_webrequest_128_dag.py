"""
Samples wmf.webrequest (1/128), formats it, and loads it to Druid.

The loading is done with 2 DAGs: an hourly one and a daily one.
The hourly DAG loads data to Druid as soon as it's available in Hive.
The daily DAG waits for a full day of data to be available in Hive,
and loads it all to Druid as a single daily segment (more efficient).
If there were hourly segments in Druid already, it will override them.
Use the daily DAG for back-filling and re-runs.
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    dataset,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.druid import HiveToDruidOperator
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

props = DagProperties(
    # DAG start dates.
    hourly_start_date=datetime(2023, 4, 25, 12),
    daily_start_date=datetime(2023, 4, 25),
    # Source database and table.
    webrequest_database="wmf",
    webrequest_table="webrequest",
    # HQL query paths.
    druid_hql_directory=f"{hql_directory}/druid_load",
    hourly_dag_hql="generate_hourly_druid_webrequests.hql",
    daily_dag_hql="generate_daily_druid_webrequests.hql",
    # Intermediate (temporary) database and base directory.
    intermediate_database="tmp",
    intermediate_base_directory=f"{hadoop_name_node}/wmf/tmp/druid",
    # Coalesce partitions (number of files) for temporary tables.
    hourly_coalesce_partitions=4,
    daily_coalesce_partitions=64,
    # Druid configs common to both DAGs.
    hive_to_druid_config={
        "druid_datasource": "webrequest_sampled_128",
        "timestamp_column": "dt",
        "timestamp_format": "auto",
        "dimensions": [
            "webrequest_source",
            "hostname",
            "time_firstbyte",
            "ip",
            "http_status",
            "cache_status",
            "response_size",
            "http_method",
            "uri_host",
            "uri_path",
            "uri_query",
            "content_type",
            "referer",
            "user_agent",
            "client_port",
            "x_cache",
            "continent",
            "country_code",
            "isp",
            "as_number",
            "is_pageview",
            "is_debug",
            "tls_version",
            "tls_key_exchange",
            "tls_auth",
            "tls_cipher",
            "requestctl",
            "is_from_public_cloud",
        ],
        "metrics": [
            "hits",
            "aggregated_response_size",
            "aggregated_time_firstbyte",
        ],
        "query_granularity": "second",
        "reduce_memory": 8192,
        "hadoop_queue": "production",
        "hive_to_druid_jar": artifact("refinery-job-0.2.14-shaded.jar"),
        "temp_directory": None,  # Override for testing.
    },
    # Configs for running very simple SparkSQL code from the Skein app.
    minimalist_spark_config={
        "master": "local",
        "driver_cores": 1,
        "driver_memory": "1G",
    },
    # SLAs and alerts email.
    hourly_dag_sla=timedelta(hours=5),
    daily_dag_sla=timedelta(days=6),
    alerts_email=alerts_email,
    # If this is True, the daily DAG waits for hourly Druid segments to be present.
    # You can override it to False for using the daily DAG to back-fill or re-run.
    wait_for_druid_segments=True,
)

default_tags = ["from_hive", "to_druid", "uses_hql", "uses_spark", "requires_wmf_webrequest"]

# Hourly DAG.
with create_easy_dag(
    dag_id="druid_load_webrequest_sampled_128_hourly",
    doc_md="Samples wmf.webrequest (1/128), formats it, and loads it to Druid hourly.",
    start_date=props.hourly_start_date,
    schedule="@hourly",
    tags=["hourly"] + default_tags,
    sla=props.hourly_dag_sla,
    email=props.alerts_email,
) as hourly_dag:
    # Table and directory where to store the temporary data to be loaded to Druid.
    intermediate_table = "tmp_{{dag.dag_id}}_{{data_interval_start | to_ds_hour_nodash}}"
    intermediate_directory = (
        props.intermediate_base_directory + "/{{dag.dag_id}}/{{data_interval_start | to_ds_hour_nodash}}"
    )

    sensor = dataset("hive_wmf_webrequest_all_sources").get_sensor_for(hourly_dag)

    formatter = SparkSqlOperator(
        task_id="format_data_to_load",
        sql=f"{props.druid_hql_directory}/{props.hourly_dag_hql}",
        query_parameters={
            "source_table": f"{props.webrequest_database}.{props.webrequest_table}",
            "destination_table": f"{props.intermediate_database}.{intermediate_table}",
            "destination_directory": intermediate_directory,
            "coalesce_partitions": props.hourly_coalesce_partitions,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
            "hour": "{{data_interval_start.hour}}",
        },
        driver_cores=2,
        driver_memory="4G",
        executor_cores=4,
        executor_memory="8G",
        conf={
            "spark.dynamicAllocation.maxExecutors": "16",
            "spark.executor.memoryOverhead": 2048,
        },
    )

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        database=props.intermediate_database,
        table=intermediate_table,
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_end | to_ds_hour}}",
        segment_granularity="hour",
        num_shards=2,
        **props.hive_to_druid_config,
    )

    hive_cleaner = SparkSqlOperator(
        task_id="remove_temporary_table",
        sql=f"DROP TABLE IF EXISTS {props.intermediate_database}.{intermediate_table};",
        **props.minimalist_spark_config,
    )

    hdfs_cleaner = SimpleSkeinOperator(
        task_id="remove_temporary_directory",
        script=f"hdfs dfs -rm -r {intermediate_directory}",
    )

    sensor >> formatter >> loader >> [hive_cleaner, hdfs_cleaner]


# Daily DAG.
with create_easy_dag(
    dag_id="druid_load_webrequest_sampled_128_daily",
    doc_md="Samples wmf.webrequest (1/128), formats it, and loads it to Druid daily.",
    start_date=props.daily_start_date,
    schedule="@daily",
    tags=["daily"] + default_tags,
    sla=props.daily_dag_sla,
    email=props.alerts_email,
) as daily_dag:
    # Table and directory where to store the temporary data to be loaded to Druid.
    intermediate_table = "tmp_{{dag.dag_id}}_{{data_interval_start|to_ds_nodash}}"
    intermediate_directory = props.intermediate_base_directory + "/{{dag.dag_id}}/{{data_interval_start|to_ds_nodash}}"

    hive_sensor = dataset("hive_wmf_webrequest_all_sources").get_sensor_for(daily_dag)

    # Usually this DAG waits for Druid hourly segments to be present.
    # This way we avoid race conditions of the 2 DAGS (hourly and daily)
    # trying to load into the same datasource at the same time.
    # But this property can be overriden, whenver we want to use this
    # dag to do re-runs or back-filling historical data.
    if props.wait_for_druid_segments:
        druid_sensor = dataset("druid_webrequest_sampled_128").get_sensor_for(daily_dag)

    formatter = SparkSqlOperator(
        task_id="format_data_to_load",
        sql=f"{props.druid_hql_directory}/{props.daily_dag_hql}",
        query_parameters={
            "source_table": f"{props.webrequest_database}.{props.webrequest_table}",
            "destination_table": f"{props.intermediate_database}.{intermediate_table}",
            "destination_directory": intermediate_directory,
            "coalesce_partitions": props.daily_coalesce_partitions,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
        driver_cores=2,
        driver_memory="4G",
        executor_cores=4,
        executor_memory="8G",
        conf={
            "spark.dynamicAllocation.maxExecutors": "64",
            "spark.executor.memoryOverhead": 2048,
        },
    )

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        database=props.intermediate_database,
        table=intermediate_table,
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_end | to_ds_hour}}",
        segment_granularity="day",
        num_shards=32,
        **props.hive_to_druid_config,
    )

    hive_cleaner = SparkSqlOperator(
        task_id="remove_temporary_table",
        sql=f"DROP TABLE IF EXISTS {props.intermediate_database}.{intermediate_table};",
        **props.minimalist_spark_config,
    )

    hdfs_cleaner = SimpleSkeinOperator(
        task_id="remove_temporary_directory",
        script=f"hdfs dfs -rm -r {intermediate_directory}",
    )

    hive_sensor >> formatter >> loader >> [hive_cleaner, hdfs_cleaner]
    if props.wait_for_druid_segments:
        druid_sensor >> formatter

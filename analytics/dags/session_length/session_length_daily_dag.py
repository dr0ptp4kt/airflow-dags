"""
### Creating the session length daily dataset
* Extracting data from the client session ticks dataset (partitioned hourly)
* Recreating virtual sessions by reindexing the data (As SessionId is not here.)
* Counting the number sessions per length
* Loading the result into a Hive table

The resulting dataset is:
* partitioned by year, month, day
* located in /wmf/data/event/mediawiki_client_session_tick/year=2022/month=3/day=8/
"""

from datetime import datetime, timedelta

from airflow import DAG

from analytics.config.dag_config import default_args, hql_directory
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor
from wmf_airflow_common.templates.time_filters import filters

dag_id = "session_length_daily"
var_props = VariableProperties(f"{dag_id}_config")
source_table = "event.mediawiki_client_session_tick"

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 6, 14)),
    schedule="@daily",
    tags=["daily", "from_hive", "to_hive", "uses_hql", "requires_event_mediawiki_client_session_tick"],
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(hours=6),
        },
    ),
    user_defined_filters=filters,
) as dag:
    sensor = RangeHivePartitionSensor(
        task_id="wait_for_session_tick",
        table_name=source_table,
        from_timestamp="{{data_interval_start | start_of_current_day}}",
        # As the input data set is generated by Refine, the presence of an
        # input partition does not guarantee that the data is complete. Thus,
        # we wait for 2 extra hours, to ensure Refine finishes processing the
        # needed partitions.
        to_timestamp="{{data_interval_start | start_of_next_day | add_hours(2)}}",
        granularity="@hourly",
        pre_partitions=["datacenter=eqiad"],
    )

    etl = SparkSqlOperator(
        task_id="process_session_length",
        sql=var_props.get("hql_path", f"{hql_directory}/session_length/daily.hql"),
        query_parameters={
            "source_table": source_table,
            "destination_table": var_props.get("destination_table", "wmf.session_length_daily"),
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
        },
        conf={"spark.dynamicAllocation.maxExecutors": 32},
    )

    sensor >> etl

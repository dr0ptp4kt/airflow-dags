"""
This job moves referer data from wmf.pageview_actor
to the wmf.referrer_daily table and archives referrer data.

Note
* This job reads from the pageview_actor table
* and runs the compute_referer_daily.hql query
* which loads the wmf_traffic.referrer table
* and then runs the compute_referer_archive_daily.hql
* which drops the result into a temporary folder
  /wmf/tmp/analytics/referrer-,
* and then archived into /wmf/data/archive/referrer/daily/referrals-for-{date}
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    archive_directory,
    create_easy_dag,
    dataset,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

dag_id = "referrer_daily"

props = DagProperties(
    start_date=datetime(2023, 5, 15),
    referrer_daily_table="wmf_traffic.referrer_daily",
    temporary_base_directory=f"{hadoop_name_node}/wmf/tmp/analytics/referrer-{dag_id}",
    # Referer generation parameters
    compute_referer_hql=f"{hql_directory}/referrer/compute_referer_daily.hql",
    pageview_actor_table="wmf.pageview_actor",
    countries_table="canonical_data.countries",
    min_num_daily_referrals=500,
    coalesce_partitions=1,
    # Referer archive parameter
    compute_referer_archive_hql=f"{hql_directory}/referrer/compute_referer_archive_daily.hql",
    # Move archive to folder parameter
    archive_file=f"{archive_directory}/referrer/daily/referrals-for-{{{{data_interval_start|to_ds}}}}.tsv",
    # SLAs and alerts email.
    dag_sla=timedelta(hours=6),
    alerts_email=alerts_email,
)

# Daily DAG.
with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    tags=[
        "daily",
        "from_hive",
        "to_hdfs",
        "to_iceberg",
        "uses_archiver",
        "uses_hql",
        "requires_wmf_pageview_actor",
    ],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:
    temporary_directory = f"{props.temporary_base_directory}/{{{{data_interval_start|to_ds_nodash}}}}"

    sensor = dataset("hive_wmf_pageview_actor").get_sensor_for(dag)

    run_referer_hql = SparkSqlOperator(
        task_id="compute_referer",
        sql=props.compute_referer_hql,
        query_parameters={
            "source_table": props.pageview_actor_table,
            "countries_table": props.countries_table,
            "coalesce_partitions": props.coalesce_partitions,
            "referer_daily_destination_table": props.referrer_daily_table,
            "min_num_daily_referrals": props.min_num_daily_referrals,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
    )

    run_referer_archive_hql = SparkSqlOperator(
        task_id="compute_referer_archive",
        sql=props.compute_referer_archive_hql,
        query_parameters={
            "referer_archive_source_table": props.referrer_daily_table,
            "destination_directory": temporary_directory,
            "day": "{{data_interval_start|to_ds}}",
        },
    )

    move_referer_data_to_archive = HDFSArchiveOperator(
        task_id="move_referer_data_to_archive",
        source_directory=temporary_directory,
        archive_file=props.archive_file,
        expected_filename_ending=".csv",
        check_done=True,
    )

    sensor >> run_referer_hql >> run_referer_archive_hql >> move_referer_data_to_archive

"""
Loads Hive pageview per article into Cassandra

The loading is done with 1 daily dag
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    create_easy_cassandra_loading_dag,
    dataset,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

props = DagProperties(
    # Data source and destination.
    hive_pageview_table="wmf.pageview_hourly",
    hive_disallowed_cassandra_articles_table="wmf.disallowed_cassandra_articles",
    cassandra_pageview_per_article_table="aqs.local_group_default_T_pageviews_per_article_flat.data",
    # DAG start date
    daily_dag_start_date=datetime(2023, 7, 18),
    # HQL query path
    daily_dag_hql=f"{hql_directory}/cassandra/daily/load_cassandra_pageview_per_article_daily.hql",
    # SLA and alerts email.
    daily_dag_sla=timedelta(hours=10),
    alerts_email=alerts_email,
)


with create_easy_cassandra_loading_dag(
    dag_id="cassandra_load_pageview_per_article_daily",
    doc_md="Loads Hive pageview per article data in cassandra daily",
    start_date=props.daily_dag_start_date,
    schedule="@daily",
    tags=[
        "daily",
        "from_hive",
        "to_cassandra",
        "uses_hql",
        "requires_wmf_disallowed_cassandra_articles",
        "requires_wmf_pageview_hourly",
    ],
    sla=props.daily_dag_sla,
    email=props.alerts_email,
) as daily_dag:
    sensor = dataset("hive_wmf_pageview_hourly").get_sensor_for(daily_dag)

    etl = SparkSqlOperator(
        task_id="load_cassandra",
        sql=props.daily_dag_hql,
        query_parameters={
            "source_table": props.hive_pageview_table,
            "disallowed_cassandra_articles_table": props.hive_disallowed_cassandra_articles_table,
            "destination_table": props.cassandra_pageview_per_article_table,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            # Setting coalesce_partitions to 6 is important
            # as it defines how many parallel loaders we use for cassandra.
            # We currently use 6 as there are 6 cassandra hosts
            "coalesce_partitions": 6,
        },
        # Spark resources configuration needs to be tweaked for this job
        # as it processes relatively big data
        executor_memory="8G",
        executor_cores=2,
        driver_memory="8G",
        driver_cores=2,
        conf={
            # Hack: We don't want to override default_args["conf"] already set,
            # we just want to add to them, so we reset them in our override.
            **daily_dag.default_args["conf"],
            "spark.dynamicAllocation.maxExecutors": 64,
            "spark.executor.memoryOverhead": 2048,
        },
    )

    sensor >> etl

"""

## This job checks Mediawiki history's new snapshot of current month and compares
## with the previous snapshot from previous month.
Email alert is sent if errors are found.
It checks the following Mediawiki-history:
* user_history,
* page_history,
* denormalized_history

It also repairs the hive tables of mediawiki-history by adding partition.

List of variables changed for testing:

    start_date
    user_history_checked
    user_history_table_location
    user_history_partitioned
    page_history_checked
    page_history_table_location
    page_history_partitioned
    denormalized_history_checked
    denormalized_history_table_location
    denormalized_history_partitioned
    refinery_job_jar
    hql_path
    mw_user_history_table
    mw_page_history_table
    mw_denormalized_history_table
    job_report_recipients
    "default_args":{"owner":"analytics-privatedata","email":"snwoko@wikimedia.org"}
"""

from datetime import datetime, timedelta
from typing import Any

from airflow import DAG
from airflow.decorators import task
from airflow.operators.email import EmailOperator
from airflow.utils.edgemodifier import Label

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    default_args,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.fail import FailOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator, SparkSubmitOperator
from wmf_airflow_common.operators.url import (
    URLDeleteOperator,
    URLTouchOperator,
    directory_has_size,
)
from wmf_airflow_common.sensors.url import URLSensor, fsspec_exists
from wmf_airflow_common.templates.time_filters import filters

dag_id = "mediawiki_history_check_denormalize"
var_props = VariableProperties(f"{dag_id}_config")

new_snapshot = "{{data_interval_start | to_ds_month}}"
previous_snapshot = "{{data_interval_start | start_of_previous_month | to_ds_month}}"

mw_directory = var_props.get("mw_directory", f"{hadoop_name_node}/wmf/data/wmf/mediawiki")
check_errors_folder = var_props.get(
    "check_errors_folder", f"{mw_directory}/history_check_errors/snapshot={new_snapshot}"
)
user_history_location = f"{mw_directory}/user_history/snapshot={new_snapshot}"
page_history_location = f"{mw_directory}/page_history/snapshot={new_snapshot}"
denormalized_history_location = f"{mw_directory}/history/snapshot={new_snapshot}"

# mw history tables
mw_user_history_table = var_props.get("mw_user_history_table", "wmf.mediawiki_user_history")
mw_page_history_table = var_props.get("mw_page_history_table", "wmf.mediawiki_page_history")
mw_denormalized_history_table = var_props.get("mw_denormalized_history_table", "wmf.mediawiki_history")

# add partition hql script path
add_partition_script = var_props.get("hql_path", f"{hql_directory}/utils/hive/add_partition.hql")

# email to send job reports
job_report_recipients = var_props.get("job_report_recipients", alerts_email)

# Mediawiki History Checker job configurations:
#   Number of wikis to check (by decreasing edit activity)
wikis_to_check = var_props.get("num_wiki", 50)
# Minimum accepted value for any events-difference-ratio metrics
# Growth is expected, we accept a small max decrease of 1%
min_events_growth = var_props.get("min_events_growth", -0.01)
# Maximum accepted value for any event-difference-ratio metric
# Since we look at large-enough wikis, saccepted variability is 100%, not more
# (small wikis can exhibit very high variabilities)
max_events_growth = var_props.get("max_events_growth", 1.0)
# Maximum number of errors-rows ratio (per dataset, user, page or denorm)
# Only up to 5% of error rows is accepted
wrongs_rows_ratio = var_props.get("wrongs_rows_ratio", 0.05)
spark_num_partitions = var_props.get("spark_num_partitions", 1024)

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2023, 3, 1)),
    schedule="@monthly",
    tags=[
        "monthly",
        "from_hive",
        "to_hdfs",
        "uses_hql",
        "uses_spark",
        "uses_url_touch",
        "requires_wmf_mediawiki_denormalized_history",
        "requires_wmf_mediawiki_page_history",
        "requires_wmf_mediawiki_user_history",
    ],
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(days=4),
        },
    ),
    user_defined_filters=filters,
) as dag:

    @task.branch(task_id="check_user_history_result")
    def check_user_history_result_branch(**kwargs: Any):
        user_history_result_dir = kwargs["user_history_error"]

        if (fsspec_exists(user_history_result_dir)) and directory_has_size(user_history_result_dir):
            return ["mediawiki_user_history_check_error"]
        else:
            return ["mark_user_history_checked"]

    @task.branch(task_id="check_page_history_result")
    def check_page_history_result_branch(**kwargs: Any):
        page_history_result_dir = kwargs["page_history_error"]

        if (fsspec_exists(page_history_result_dir)) and directory_has_size(page_history_result_dir):
            return ["mediawiki_page_history_check_error"]
        else:
            return ["mark_page_history_checked"]

    @task.branch(task_id="check_denormalized_history_result")
    def check_denormalized_history_result_branch(**kwargs: Any):
        denormalized_history_result_dir = kwargs["denormalized_history_error"]

        if (fsspec_exists(denormalized_history_result_dir)) and directory_has_size(denormalized_history_result_dir):
            return ["mediawiki_denormalized_history_check_error"]
        else:
            return ["mark_denormalized_history_checked"]

    # Creates a task reporting an error.
    def fail_check(stage: str) -> FailOperator:
        return FailOperator(
            task_id=f"{stage}_check_error",
            message=(
                f"Check of {stage} snapshot {new_snapshot} has generated "
                f"a non-empty error file at {check_errors_folder}."
            ),
        )

    # This job doesn't depend on hive partitions being created
    # Rather,it relies on the hive table data since it reads files.
    all_history_location = [user_history_location, page_history_location, denormalized_history_location]
    sensors = [
        URLSensor(
            task_id=f"check_{history_location.split('/')[-2]}_dataset_files",
            url=history_location + "/_SUCCESS",
            poke_interval=timedelta(hours=1).total_seconds(),
        )
        for history_location in all_history_location
    ]

    # Task to delete errors folder if folder is present
    delete_preexisting_error_folder = URLDeleteOperator(
        task_id="delete_preexisting_error_folder", url=check_errors_folder, if_exists=True, recursive=True
    )

    ###
    # Task to check user_history new snapshot versus previous snapshot for errors.
    check_user_history = SparkSubmitOperator(
        task_id="check_user_history",
        trigger_rule="none_failed_min_one_success",
        executor_memory="16G",
        executor_cores=2,
        driver_memory="16G",
        conf={
            "spark.dynamicAllocation.maxExecutors": 90,
        },
        application=var_props.get("refinery_job_jar", artifact("refinery-job-0.2.24-shaded.jar")),
        java_class="org.wikimedia.analytics.refinery.job.mediawikihistory.MediawikiHistoryChecker",
        application_args=[
            "--mediawiki-history-base-path",
            mw_directory,
            "--previous-snapshot",
            previous_snapshot,
            "--new-snapshot",
            new_snapshot,
            "--wikis-to-check",
            wikis_to_check,
            "--num-partitions",
            spark_num_partitions,
            "--min-events-growth-threshold",
            min_events_growth,
            "--max-events-growth-threshold",
            max_events_growth,
            "--wrong-rows-ratio-threshold",
            wrongs_rows_ratio,
            "--check-user-history",
        ],
    )
    # Branching task to send error message if error exists in history_error folder
    # after performing check_user_history task.
    # Otherwise continue
    check_user_error = check_user_history_result_branch(user_history_error=check_errors_folder)

    # Task to fail the check for the user history
    fail_user_check = fail_check("mediawiki_user_history")

    # Task to mark user_history location as Checked
    # after check_user_history task completes with no error
    mark_user_checked = URLTouchOperator(
        task_id="mark_user_history_checked",
        url=var_props.get("user_history_checked", (user_history_location + "/_CHECKED")),
    )

    # Remember to copy add_partition.hql to hql directory
    # Task to add new partition to mediawiki user history table
    partition_user_table = SparkSqlOperator(
        task_id="add_mw_user_history_table_partition",
        sql=add_partition_script,
        query_parameters={
            "table": mw_user_history_table,
            "partition_spec": f"snapshot='{new_snapshot}'",
            "location": var_props.get("user_history_table_location", user_history_location),
        },
    )

    # Task to mark user_history table location as partitioned
    mark_user_partitioned = URLTouchOperator(
        task_id="mark_user_history_hive_done",
        url=var_props.get("user_history_partitioned", (user_history_location + "/_PARTITIONED")),
    )

    ####
    # Check page_history new snapshot versus previous snapshot for errors.
    check_page_history = SparkSubmitOperator(
        task_id="check_page_history",
        executor_memory="16G",
        executor_cores=2,
        driver_memory="16G",
        conf={
            "spark.dynamicAllocation.maxExecutors": 90,
        },
        application=var_props.get("refinery_job_jar", artifact("refinery-job-0.2.24-shaded.jar")),
        java_class="org.wikimedia.analytics.refinery.job.mediawikihistory.MediawikiHistoryChecker",
        application_args=[
            "--mediawiki-history-base-path",
            mw_directory,
            "--previous-snapshot",
            previous_snapshot,
            "--new-snapshot",
            new_snapshot,
            "--wikis-to-check",
            wikis_to_check,
            "--num-partitions",
            spark_num_partitions,
            "--min-events-growth-threshold",
            min_events_growth,
            "--max-events-growth-threshold",
            max_events_growth,
            "--wrong-rows-ratio-threshold",
            wrongs_rows_ratio,
            "--check-page-history",
        ],
    )

    # Branching task to send error message if error exists in history_error folder
    # after performing check_page_history task.
    # Otherwise continue
    check_page_error = check_page_history_result_branch(page_history_error=check_errors_folder)

    # Task to fail the check for the page history
    fail_page_check = fail_check("mediawiki_page_history")

    # Task to mark page_history location as Checked
    # after check_page_history task completes with no error
    mark_page_checked = URLTouchOperator(
        task_id="mark_page_history_checked",
        url=var_props.get("page_history_checked", (page_history_location + "/_CHECKED")),
    )

    # Task to add new partition to mediawiki page history table
    partition_page_table = SparkSqlOperator(
        task_id="add_mw_page_history_table_partition",
        sql=add_partition_script,
        query_parameters={
            "table": mw_page_history_table,
            "partition_spec": f"snapshot='{new_snapshot}'",
            "location": var_props.get("page_history_table_location", page_history_location),
        },
    )

    # Task to mark page_history table location as partitioned
    mark_page_partitioned = URLTouchOperator(
        task_id="mark_page_history_hive_done",
        url=var_props.get("page_history_partitioned", (page_history_location + "/_PARTITIONED")),
    )

    #####
    # Check denormalized_history new snapshot versus previous snapshot for errors.
    check_denormalized_history = SparkSubmitOperator(
        task_id="check_denormalized_history",
        executor_memory="16G",
        executor_cores=2,
        driver_memory="16G",
        conf={
            "spark.dynamicAllocation.maxExecutors": 90,
        },
        application=var_props.get("refinery_job_jar", artifact("refinery-job-0.2.24-shaded.jar")),
        java_class="org.wikimedia.analytics.refinery.job.mediawikihistory.MediawikiHistoryChecker",
        application_args=[
            "--mediawiki-history-base-path",
            mw_directory,
            "--previous-snapshot",
            previous_snapshot,
            "--new-snapshot",
            new_snapshot,
            "--wikis-to-check",
            wikis_to_check,
            "--num-partitions",
            spark_num_partitions,
            "--min-events-growth-threshold",
            min_events_growth,
            "--max-events-growth-threshold",
            max_events_growth,
            "--wrong-rows-ratio-threshold",
            wrongs_rows_ratio,
            "--check-denormalized-history",
        ],
    )

    # Branching task to send error message if error exists in history_error folder
    # after performing check_denormalized_history task.
    # Otherwise continue
    check_denormalized_error = check_denormalized_history_result_branch(denormalized_history_error=check_errors_folder)

    # Task to fail the check for the denormalized history
    fail_denormalized_check = fail_check("mediawiki_denormalized_history")

    # Task to mark denormalized_history location as Checked
    # after check_denormalized_history task completes with no error
    mark_denormalized_checked = URLTouchOperator(
        task_id="mark_denormalized_history_checked",
        url=var_props.get("denormalized_history_checked", (denormalized_history_location + "/_CHECKED")),
    )

    # Task to add mediawiki denormalized_history table partition.
    partition_denormalized_table = SparkSqlOperator(
        task_id="add_mw_denormalized_history_table_partition",
        sql=add_partition_script,
        query_parameters={
            "table": mw_denormalized_history_table,
            "partition_spec": f"snapshot='{new_snapshot}'",
            "location": var_props.get("denormalized_history_table_location", denormalized_history_location),
        },
    )

    # Task to mark denormalized_history table location as partitioned
    mark_denormalized_partitioned = URLTouchOperator(
        task_id="mark_denormalized_history_hive_done",
        url=var_props.get("denormalized_history_partitioned", (denormalized_history_location + "/_PARTITIONED")),
    )

    # Task to send success email after checking of mediawiki_history new snapshot
    # is completed successfully with no errors
    success_mail = EmailOperator(
        task_id="send_success_email",
        to=job_report_recipients,
        subject=f"Mediawiki_history for {new_snapshot} now available",
        html_content=(
            f"The job calculating and checking the {new_snapshot} "
            "mediawiki_history snapshot has finished successfully and the data is now available :)<br/>"
            "Job details:<br/>"
            "- Job: {{ dag.dag_id }}<br/>"
            "- Date: {{ ds }}<br/>"
            "-- Airflow"
        ),
    )

    sensors >> delete_preexisting_error_folder >> check_user_history >> check_user_error

    # Check user history branches
    check_user_error >> Label("User Errors Found") >> fail_user_check
    (
        check_user_error
        >> Label("No User Error")
        >> mark_user_checked
        >> partition_user_table
        >> mark_user_partitioned
        >> check_page_history
        >> check_page_error
    )

    # Check page history branches
    check_page_error >> Label("Page Errors Found") >> fail_page_check
    (
        check_page_error
        >> Label("No Page Error")
        >> mark_page_checked
        >> partition_page_table
        >> mark_page_partitioned
        >> check_denormalized_history
        >> check_denormalized_error
    )

    # Check denormalized history branches
    check_denormalized_error >> Label("Denormalized Errors Found") >> fail_denormalized_check
    (
        check_denormalized_error
        >> Label("No Denormalized Error")
        >> mark_denormalized_checked
        >> partition_denormalized_table
        >> mark_denormalized_partitioned
        >> success_mail
    )

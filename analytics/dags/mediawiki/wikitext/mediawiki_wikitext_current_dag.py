"""
This job generates parquet mediawiki wikitext current from XML-dumps revisions
and add hive partitions accordingly.
"""

from datetime import datetime, timedelta

from airflow import DAG

from analytics.config.dag_config import (
    artifact,
    default_args,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator, SparkSubmitOperator
from wmf_airflow_common.operators.url import URLTouchOperator
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.templates.time_filters import filters

dag_id = "mediawiki_wikitext_current"
var_props = VariableProperties(f"{dag_id}_config")

# Dumps generated at month YYYYMM contains data for month MM-1
# Hence we would sense for a input XML dump with one month shift ie YYYY(MM+1)DD
input_dump_date = "{{data_interval_start| start_of_next_month | to_ds_nodash}}"
pages_meta_current_xml_dump = f"{hadoop_name_node}/wmf/data/raw/mediawiki/dumps/pages_meta_current/{input_dump_date}"

# we would save the output parquet with snapshot=YYYY-MM
wikitext_current_snapshot = "{{data_interval_start | to_ds_month}}"
mw_wikitext_current_location = var_props.get(
    "mw_wikitext_current_path",
    f"{hadoop_name_node}/wmf/data/wmf/mediawiki/wikitext/current/snapshot={wikitext_current_snapshot}",
)
# output table
mw_wikitext_current_table = var_props.get("output_table", "wmf.mediawiki_wikitext_current")

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2023, 3, 1)),
    schedule="@monthly",
    tags=["monthly", "from_hdfs", "to_hive", "uses_hql", "uses_spark", "requires_pages_meta_current_xml_dump"],
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(days=24),
        },
    ),
    user_defined_filters=filters,
) as dag:
    sensor = URLSensor(
        task_id="wait_for_pages_meta_current_xml_dump",
        url=pages_meta_current_xml_dump + "/_SUCCESS",
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    xml_to_parquet = SparkSubmitOperator(
        task_id="convert_current_xml_to_parquet",
        executor_memory="32G",
        executor_cores=4,
        driver_memory="8G",
        conf={
            "spark.dynamicAllocation.maxExecutors": 32,
            "spark.hadoop.io.compression.codecs": "org.wikimedia.analytics.refinery.spark.bzip2.CorrectedBZip2Codec",
        },
        application=var_props.get("refinery_job_jar", artifact("refinery-job-0.2.24-shaded.jar")),
        java_class="org.wikimedia.analytics.refinery.job.mediawikihistory.MediawikiXMLDumpsConverter",
        application_args={
            "--xml_dumps_base_path": pages_meta_current_xml_dump,
            "--output_base_path": mw_wikitext_current_location,
            "--max_parallel_jobs": 128,
            "--output_format": "avro",
        },
    )

    repair = SparkSqlOperator(
        task_id="repair_mediawiki_wikitext_current_table",
        sql=var_props.get(
            "hql_path",
            f"{hql_directory}/utils/repair_partitions.hql",
        ),
        query_parameters={"table": mw_wikitext_current_table},
    )

    mark_partitioned = URLTouchOperator(
        task_id="write_mw_wikitext_current_table_partitioned_file",
        url=var_props.get("partitioned_path", f"{mw_wikitext_current_location}/_PARTITIONED"),
    )

    sensor >> xml_to_parquet >> repair >> mark_partitioned

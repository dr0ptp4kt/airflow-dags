"""

# This job checks for the existence sqooped raw data in hdfs
# then automatically repairs the partitions on the corresponding hive table for each sqooped dataset
#  and creates a "_PARTITIONED" file when done

"""

from datetime import datetime, timedelta

from airflow import DAG

from analytics.config.dag_config import default_args, hadoop_name_node, hql_directory
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.operators.url import URLTouchOperator
from wmf_airflow_common.sensors.url import URLSensor

# table names are prefixed with mediawiki whereas pathnames are not , have a statement to resolve

dag_id = "mediawiki_history_load"
var_props = VariableProperties(f"{dag_id}_config")

public_tables = [
    "archive",
    "category",
    "categorylinks",
    "change_tag",
    "change_tag_def",
    "content",
    "content_models",
    "externallinks",
    "image",
    "imagelinks",
    "ipblocks",
    "ipblocks_restrictions",
    "iwlinks",
    "langlinks",
    "linktarget",
    "logging",
    "page",
    "page_props",
    "page_restrictions",
    "pagelinks",
    "redirect",
    "revision",
    "slot_roles",
    "slots",
    "templatelinks",
    "user",
    "user_groups",
    "user_properties",
    "wbc_entity_usage",
]

private_tables = ["actor", "comment", "cu_changes", "watchlist"]

# the table below has been separated because its file path is different from the rest
base_level_tables = ["project_namespace_map"]

tables = public_tables + private_tables + base_level_tables


# Most all tables referenced above have top-level hive partitions that follow the pattern 'snapshot=YYYY-MM'.
# However, table 'cu_changes' follows a different pattern 'month=YYYY-MM'.
# Thus, to reuse the code below, this function returns the right top-level hive partition name.
def partition_for(table_name):
    if table_name == "cu_changes":
        return "month"
    else:
        return "snapshot"


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 6, 1)),
    schedule="@monthly",
    tags=[
        "monthly",
        "from_hive",
        "to_hive",
        "uses_hql",
        "uses_url_touch",
        # requires alot of tables refer to public_tables
    ],
    # Setting concurrency value to avoid overloading the metastore
    # in the case where too many repair tables are executed at once
    max_active_tasks=10,
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(days=5),
        },
    ),
) as dag:
    for table in tables:
        if table in public_tables:
            base_path = var_props.get("public_base_path", "/wmf/data/raw/mediawiki/tables")
            table_prefix = "mediawiki_"
        elif table in private_tables:
            base_path = var_props.get("private_base_path", "/wmf/data/raw/mediawiki_private/tables")
            table_prefix = "mediawiki_private_"
        else:  # table in base_level_tables
            base_path = var_props.get("level_base_path", "/wmf/data/raw/mediawiki")
            table_prefix = "mediawiki_"

        url_path = f"{hadoop_name_node}{base_path}/{table}"

        date_month = '{{data_interval_start.strftime("%Y-%m")}}'

        sensor = URLSensor(
            task_id=f"wait_for_{table}_data",
            url=url_path + f"/{partition_for(table)}={date_month}/_SUCCESS",
            poke_interval=timedelta(hours=1).total_seconds(),
        )

        repair = SparkSqlOperator(
            task_id=f"repair_{table}_table",
            sql=var_props.get(
                "hql_path",
                f"{hql_directory}/utils/repair_partitions.hql",
            ),
            query_parameters={"table": f"wmf_raw.{table_prefix}{table}"},
            sla=timedelta(days=7),
        )

        update = URLTouchOperator(
            task_id=f"write_{table}_table_partitioned_file",
            url=var_props.get("destination_path", (url_path + f"/{partition_for(table)}={date_month}/_PARTITIONED")),
        )

        sensor >> repair >> update

"""
This job runs the hql that aggregates mediawiki history reduced
from the wmf.mediawiki_history. It deletes the error file from the
previous run, if it exists, then checks mediawiki_history_reduced
new snapshot versus previous snapshot for errors. If there are no
errors it indexes to druid.

Note
This job waits for wmf.mediawiki_history data for the month
to be available before running the aggregate hql query.
output of the hql is then put in the wmf.mediawiki_history_reduced table.
"""

from datetime import datetime, timedelta

from airflow.decorators import task

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    dataset,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.druid import HiveToDruidOperator
from wmf_airflow_common.operators.fail import FailOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator, SparkSubmitOperator
from wmf_airflow_common.operators.url import URLDeleteOperator, directory_has_size
from wmf_airflow_common.sensors.url import fsspec_exists

props = DagProperties(
    # DAG start date.
    start_date=datetime(2023, 4, 1),
    # Source database and table.
    mwh_reduced_table="wmf.mediawiki_history_reduced",
    mediawiki_history_table="wmf.mediawiki_history",
    mw_project_namespace_map_table="wmf_raw.mediawiki_project_namespace_map",
    # HQL query properties and Path
    mwh_reduced_coalesce_partitions=4096,
    mwh_reduced_shuffle_partitions=4096,
    mwh_reduced_min_events_growth=-0.01,
    mw_directory=f"{hadoop_name_node}/wmf/data/wmf/mediawiki",
    new_snapshot="{{data_interval_start | to_ds_month}}",
    previous_snapshot="{{data_interval_start | start_of_previous_month | to_ds_month}}",
    hql_path=f"{hql_directory}/mediawiki/history/compute_mediawiki_history_reduced.hql",
    # Checker refinery jar
    refinery_job_jar=artifact("refinery-job-0.2.24-shaded.jar"),
    # Druid configs
    hive_to_druid_config={
        # This job needs to index to  the druid-public cluster
        "druid_host": "druid1010.eqiad.wmnet",
        # Usual HiveToDruid parameters
        "database": "wmf",
        "table": "mediawiki_history_reduced",
        "druid_datasource": "mediawiki_history_reduced_{{data_interval_start.format('YYYY_MM')}}",
        "timestamp_column": "event_timestamp",
        "timestamp_format": "yyyy-MM-dd HH:mm:ss.S",
        "dimensions": [
            "project",
            "event_entity",
            "event_type",
            "user_text",
            "user_type",
            "page_title",
            "page_namespace",
            "page_type",
            "other_tags",
            "revisions",
            "text_bytes_diff",
            "text_bytes_diff_abs",
        ],
        "metrics": ["revisions_sum", "text_bytes_diff_sum", "text_bytes_diff_abs_sum"],
        "count_metric_name": "events",
        # Transforming the metric names so they can use a name
        # different from the dimension names
        "transforms": [
            "revisions as revisions_sum",
            "text_bytes_diff as text_bytes_diff_sum",
            "text_bytes_diff_abs as text_bytes_diff_abs_sum ",
        ],
        "query_granularity": "day",
        "segment_granularity": "month",
        "num_shards": 16,
        "reduce_memory": 8192,
        "hadoop_queue": "production",
        "hive_to_druid_jar": artifact("refinery-job-0.2.24-shaded.jar"),
        "temp_directory": None,  # Override for testing.
    },
    sla=timedelta(days=4),
    alerts_email=alerts_email,
)
checker_errors_folder = f"{props.mw_directory}/history_reduced_check_errors/snapshot={props.new_snapshot}"

with create_easy_dag(
    dag_id="mediawiki_history_reduced",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=["monthly", "from_hive", "to_druid", "to_hive", "uses_hql", "uses_spark", "requires_wmf_mediawiki_history"],
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    sensor = dataset("hive_wmf_mediawiki_history").get_sensor_for(dag)

    run_hql_query = SparkSqlOperator(
        task_id="generate_mediawiki_history_reduced",
        sql=props.hql_path,
        query_parameters={
            "destination_table": f"{props.mwh_reduced_table}",
            "mw_denormalized_history_table": props.mediawiki_history_table,
            "mw_project_namespace_map_table": props.mw_project_namespace_map_table,
            "coalesce_partitions": props.mwh_reduced_coalesce_partitions,
            "shuffle_partitions": props.mwh_reduced_shuffle_partitions,
            "snapshot": props.new_snapshot,
        },
        driver_memory="24G",
        driver_cores=2,
        executor_memory="24G",
        executor_cores=3,
        conf={
            "spark.dynamicAllocation.maxExecutors": 90,
            "spark.executor.memoryOverhead": "6G",
        },
    )

    delete_preexisting_checker_errors_folder = URLDeleteOperator(
        task_id="delete_preexisting_checker_errors_folder", url=checker_errors_folder, if_exists=True, recursive=True
    )

    # Task to check mediawiki_history_reduced
    # new snapshot versus previous snapshot for errors.
    run_checker = SparkSubmitOperator(
        task_id="check_mediawiki_history_reduced",
        executor_memory="24G",
        executor_cores=3,
        driver_memory="32G",
        conf={
            "spark.dynamicAllocation.maxExecutors": 80,
            "spark.yarn.executor.memoryOverhead": 6144,
        },
        application=props.refinery_job_jar,
        java_class="org.wikimedia.analytics.refinery" ".job.mediawikihistory.MediawikiHistoryChecker",
        application_args=[
            "--mediawiki-history-base-path",
            props.mw_directory,
            "--previous-snapshot",
            props.previous_snapshot,
            "--new-snapshot",
            props.new_snapshot,
            "--wikis-to-check",
            50,
            "--num-partitions",
            1024,
            "--min-events-growth-threshold",
            props.mwh_reduced_min_events_growth,
            "--max-events-growth-threshold",
            1.0,
            "--wrong-rows-ratio-threshold",
            0.05,
            "--check-reduced-history",
        ],
    )

    @task.branch(task_id="check_mediawiki_history_reduced_error_folder")
    def check_mediawiki_history_reduced_error_folder(check_errors_file):
        if (fsspec_exists(check_errors_file)) and directory_has_size(check_errors_file):
            return ["mediawiki_history_reduced_check_error"]
        else:
            return ["load_wmf_mediawiki_history_reduced_to_druid"]

    checker_result_branch = check_mediawiki_history_reduced_error_folder(check_errors_file=checker_errors_folder)

    fail_check = FailOperator(
        task_id="mediawiki_history_reduced_check_error",
        message=(
            f"Check of mediawiki_history_reduced snapshot {props.new_snapshot} has generated "
            f"a non-empty error file at {checker_errors_folder}."
        ),
    )

    index_to_druid = HiveToDruidOperator(
        task_id="load_wmf_mediawiki_history_reduced_to_druid",
        # Use a snapshot value to read data from hive
        # as since/until don't match the snapshot
        snapshot="{{data_interval_start | to_ds_month}}",
        # Harcode indexation start-date to reload full snapshot
        since="2001-01-01T00",
        until="{{data_interval_start | add_months(1) | to_ds_hour}}",
        executor_cores=2,
        executor_memory="16G",
        conf={
            "spark.dynamicAllocation.maxExecutors": 128,
            "spark.yarn.executor.memoryOverhead": 4096,
        },
        **props.hive_to_druid_config,
    )

    sensor >> run_hql_query >> delete_preexisting_checker_errors_folder
    delete_preexisting_checker_errors_folder >> run_checker >> checker_result_branch
    checker_result_branch >> [fail_check, index_to_druid]

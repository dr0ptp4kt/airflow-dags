from datetime import timedelta
import shlex
from typing import Any, List, Union
from wmf_airflow_common.operators.skein import SimpleSkeinOperator


def quote_command_args(command: str, args: List[str]) -> str:
    arg_str = ' '.join(shlex.quote(arg) for arg in args)
    return command + ' ' + arg_str


def swift_upload(
    container: str,
    source_directory: str,
    object_prefix: str,
    swift_upload_py: str = (
        'hdfs://analytics-hadoop/wmf/refinery/current/'
        'oozie/util/swift/upload/swift_upload.py'),
    swift_auth_file: str = (
        'hdfs://analytics-hadoop/user/analytics/'
        'swift_auth_analytics_admin.env'),
    event_stream: Union[bool, str] = True,
    overwrite: bool = False,
    delete_after: Union[str, timedelta] = timedelta(days=30),
    auto_version: bool = False,
    event_per_object: bool = False,
    event_service_url: str = 'https://eventgate-analytics.svc.eqiad.wmnet:4592/v1/events',
    **kwargs: Any
) -> SimpleSkeinOperator:
    if isinstance(delete_after, timedelta):
        delete_after = int(delete_after.total_seconds())

    if event_stream is True:
        event_stream = f'swift.{container}.upload-complete'
    elif event_stream is False:
        event_stream = 'false'

    return SimpleSkeinOperator(
        script=quote_command_args('python3', [
                                     'swift_upload.py',
                                     '--swift-overwrite', str(overwrite).lower(),
                                     # Script only accepts integer values,
                                     # floats or non-numeric strings will fail
                                     '--swift-delete-after', str(delete_after),
                                     '--swift-auto-version', str(auto_version).lower(),
                                     '--swift-object-prefix', object_prefix,
                                     '--event-per-object', str(event_per_object).lower(),
                                     '--event-stream', event_stream,
                                     '--event-service-url', event_service_url,
                                     'swift_auth.env',
                                     container,
                                     source_directory,
                                 ]),
        resources={ 'memory': 1024, 'vcores': 1 },
        files={
            # TODO pass files, or use a pre-built env?
            'swift_auth.env': swift_auth_file,
            'swift_upload.py': swift_upload_py,
        },
        **kwargs
    )

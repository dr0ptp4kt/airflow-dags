import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "projectview", "projectview_hourly_dag.py"]


def test_projectview_hourly_dag_test_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="projectview_hourly")
    assert dag is not None
    assert len(dag.tasks) == 4

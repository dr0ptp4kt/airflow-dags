import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "aqs", "aqs_hourly_dag.py"]


def test_aqs_hourly_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="aqs_hourly")
    assert dag is not None
    assert len(dag.tasks) == 3
    assert dag.tasks[0].partition_names == [
        "wmf.webrequest/webrequest_source=text"
        "/year={{data_interval_start.year}}"
        "/month={{data_interval_start.month}}"
        "/day={{data_interval_start.day}}"
        "/hour={{data_interval_start.hour}}"
    ]

    # Tests that the defaults from default_args are here.
    assert dag.default_args["do_xcom_push"] is False

    # Tests that the defaults from dag_config are here.
    assert dag.default_args["metastore_conn_id"] == "analytics-hive"

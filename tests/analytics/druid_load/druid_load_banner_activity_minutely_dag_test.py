import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "druid_load", "druid_load_banner_activity_minutely_dag.py"]


def test_druid_load_banner_activity_minutely_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    daily_dag = dagbag.get_dag(dag_id="druid_load_banner_activity_minutely_aggregated_daily")
    assert daily_dag is not None
    assert len(daily_dag.tasks) == 5
    monthly_dag = dagbag.get_dag(dag_id="druid_load_banner_activity_minutely_aggregated_monthly")
    assert monthly_dag is not None
    assert len(monthly_dag.tasks) == 5

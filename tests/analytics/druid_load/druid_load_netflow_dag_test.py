import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "druid_load", "druid_load_netflow_dag.py"]


def test_druid_load_netflow_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    hourly_dag = dagbag.get_dag(dag_id="druid_load_netflow_hourly")
    assert hourly_dag is not None
    assert len(hourly_dag.tasks) == 2
    daily_dag = dagbag.get_dag(dag_id="druid_load_netflow_daily")
    assert daily_dag is not None
    assert len(daily_dag.tasks) == 3
    sanitization_dag = dagbag.get_dag(dag_id="druid_load_netflow_sanitized")
    assert sanitization_dag is not None
    assert len(sanitization_dag.tasks) == 2
